﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using restapi_dev.Data;
using restapi_dev.Models;

namespace restapi_dev.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CuestionarioController : ControllerBase
    {

        private readonly CuestionarioContexto context;

        public CuestionarioController(CuestionarioContexto contexto)
        {
            context = contexto;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<Cuestionario>>> GetCuestionarioItems()
        {
            return await context.Cuestionario.ToListAsync();
        }


        [HttpGet("{ID_cuestionario}")]
        public async Task<ActionResult<Cuestionario>> GetCuestionarioItems(Int64 ID_cuestionario)
        {
            var CuestionarioItem = await context.Cuestionario.FindAsync(ID_cuestionario);

            if (CuestionarioItem == null)
            {
                return NotFound();
            }

            return CuestionarioItem;
        }

        [HttpPost]
        public async Task<ActionResult<IEnumerable<Cuestionario>>> PostCuestionarioItems(Cuestionario item)
        {
            context.Cuestionario.Add(item);
            await context.SaveChangesAsync();

            return null;
        }


        [HttpPut("{ID_cuestionario}")]
        public async Task<IActionResult> PutCuestionarioItem(Int64 ID_cuestionario, Cuestionario item)
        {
            if (ID_cuestionario != item.ID_cuestionario) 
            {
                return BadRequest();
            }

            context.Entry(item).State = EntityState.Modified;
            await context.SaveChangesAsync();

            return NoContent();
        }




    }
}