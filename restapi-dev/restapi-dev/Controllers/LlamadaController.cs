﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using restapi_dev.Data;
using restapi_dev.Models;

namespace restapi_dev.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LlamadaController : ControllerBase
    {
        private readonly llamada_medicoContexto context;

        public LlamadaController(llamada_medicoContexto contexto)
        {
            context = contexto;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<llamada_medico>>> GetLlamadaItems()
        {
            return await context.llamada_medico.ToListAsync();
        }

        [HttpPost]
        public async Task<ActionResult<IEnumerable<llamada_medico>>> PostLlamadaItems(llamada_medico item)
        {
            context.llamada_medico.Add(item);
            await context.SaveChangesAsync();

            return null;
        }

        [HttpDelete("{id_turno}")]
        public async Task<IActionResult>DeleteLlamadaItems(Int64 id_turno)
        {
            var llamadaItem = await context.llamada_medico.FindAsync(id_turno);

            if (llamadaItem == null) {
                return NotFound();
            }

            context.llamada_medico.Remove(llamadaItem);
            await context.SaveChangesAsync();

            return NoContent();
        }

        


    }
}