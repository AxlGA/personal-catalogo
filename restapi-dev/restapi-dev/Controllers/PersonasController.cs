﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using restapi_dev.Data;
using restapi_dev.Models;

namespace restapi_dev.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PersonasController : ControllerBase
    {
        private readonly personasContexto context;

        public PersonasController(personasContexto contexto)
        {
            context = contexto;
        }


        [HttpGet]
        public async Task<ActionResult<IEnumerable<personas>>> GetPersonasItems() 
        {
            return await context.personas.ToListAsync();
        }

    
        [HttpGet("{username2}")]
        public async Task<ActionResult<personas>> GetPersonasItems(string username2)
        {
         

            var PersonaItem = await context.personas.FirstOrDefaultAsync(x => x.username.Equals(username2));
            if (PersonaItem == null)
            {
                return NoContent();
            }
            return PersonaItem;
        }


        [HttpPost]
        public async Task<ActionResult<IEnumerable<personas>>> PostPersonasItems(personas item)
        {
            context.personas.Add(item);
            await context.SaveChangesAsync();

            return null;
        }

        
    }
}