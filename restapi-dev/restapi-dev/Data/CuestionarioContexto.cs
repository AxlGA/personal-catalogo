﻿using Microsoft.EntityFrameworkCore;
using restapi_dev.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restapi_dev.Data
{
    public class CuestionarioContexto: DbContext
    {
        public CuestionarioContexto(DbContextOptions<CuestionarioContexto>options):base(options)
        {

        }

        public DbSet<Cuestionario> Cuestionario { get; set; }
    }
}
