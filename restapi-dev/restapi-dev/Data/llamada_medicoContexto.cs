﻿using Microsoft.EntityFrameworkCore;
using restapi_dev.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restapi_dev.Data
{
    public class llamada_medicoContexto: DbContext
    {

        public llamada_medicoContexto(DbContextOptions<llamada_medicoContexto>options):base(options)
        {

        }

        public DbSet<llamada_medico> llamada_medico { get; set; }
    }
}
