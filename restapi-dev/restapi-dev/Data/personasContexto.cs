﻿using Microsoft.EntityFrameworkCore;
using restapi_dev.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace restapi_dev.Data
{
    public class personasContexto: DbContext
    {
        public personasContexto(DbContextOptions<personasContexto>options):base(options)
        {

        }
        public DbSet<personas> personas { get; set; }

    }
}
