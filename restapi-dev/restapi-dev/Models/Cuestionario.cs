﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace restapi_dev.Models
{
    public class Cuestionario
    {

        public Cuestionario()
        {
            fecha = DateTime.Now;
        }
        
    
         [Key]
        public Int64 ID_cuestionario { get; set; }
        public Int64 ID_persona { get; set; }
        public string Nombre_Paciente { get; set; }
        public string Registro_Paciente { get; set; }
        public string Formato_Edad { get; set; }
        public string Edad_Paciente { get; set; }

        public DateTime fecha { get; private set; }
        public string Pregunta_neurologico { get; set; }

        public Int64 Valor_neurologico { get; set; }

        public string Pregunta_cardiovascular { get; set; }

        public Int64 Valor_cardiovascular { get; set; }

        public string Pregunta_respiratoria { get; set; }

        public Int64 Valor_repiratorio { get; set; }

        public string Tipo_preocupacion { get; set; }

        public Int64 Valor_preocupacion { get; set; }

        public Int64 total { get; set; }
    }
}
