﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace restapi_dev.Models
{
    public class llamada_medico
    {
        [Key]
        public Int64 id_turno { get; set; }
        
        public Int64 id_persona { get; set; }
    }
}
