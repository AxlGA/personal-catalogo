﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace restapi_dev.Models
{
    public class personas
    {
        [Key]
        public Int64 id { get; set; }

       // public string nombre { get; set; }

          public byte tipo_de_persona { get; set; }
        
        public string nombres { get; set; }
          public string apellidos { get; set; }
    
        public string username { get; set; }

          public string password { get; set; }
          public string correo { get; set; }
          public string numero_telefonico { get; set; }
      
    }
}
