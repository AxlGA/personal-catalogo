﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

namespace restapi_dev.Services
{
    public class Mensaje
    {

        public Mensaje()
        {

        }

        public string correo2 = "";


        public async Task<int> Envio(string correo)
        {
            correo2 = correo;
            int var;
            BackgroundWorker bgw = new BackgroundWorker();

            bgw.DoWork += bgw_DoWork;
            bgw.RunWorkerCompleted += bgw_RunWorkerCompleted;
            bgw.RunWorkerAsync();

            Console.WriteLine("Entro a la clase de Envio");
           
            var = 16;
            return var;
        }

        public void bgw_DoWork(object sender, DoWorkEventArgs e)
        {
            // e.Result = 10;

            var message = new MimeMessage();

            message.From.Add(new MailboxAddress("EVAT Support", "nclt.pruebas@gmail.com"));

            message.To.Add(new MailboxAddress("Recuperación de Contraseña", correo2));

            message.Subject = "Recuperación de Contraseña EVAT";

            message.Body = new TextPart("plain")
            {
                Text = "Saludos del Soporte de EVAT, por favor dirijase al siguiente link http://localhost:8100/password " +
                "Gracias por utilizar EVAT"
            };

            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 587);
                client.AuthenticationMechanisms.Remove("XOAUTH2");
                client.Authenticate("nclt.pruebas", "juevesdepaches");

                client.Send(message);

                client.Disconnect(true);
            }
            Console.WriteLine("Ya termino de enviar el correo");
            e.Result = 10;
        }

        public void bgw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            Console.WriteLine("Ya finalizo la tarea");

        }


    }
}
