import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-crear-usuario',
  templateUrl: './crear-usuario.page.html',
  styleUrls: ['./crear-usuario.page.scss'],
})
export class CrearUsuarioPage implements OnInit {

  persona = {
    nombres: '',
    apellidos: '',
    username: '',
    password: '',
    numero_telefonico: '',
    correo: '',
    tipo_de_persona: 0,
  };

  key= 'Evat2020Evat';

    constructor(public http: HttpClient) { }

  ngOnInit() {
  }

    sendPostRequest() {

        this.persona.password=CryptoJS.AES.encrypt(this.persona.password.trim(),this.key.trim()).toString();
        var headers = new HttpHeaders();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        this.http.post('https://localhost:44347/api/personas', this.persona, { headers: headers }).subscribe();
    }
    
  onSubmitTemplate() {
    console.log('Form submit');
    console.log(this.persona);
  }

}
