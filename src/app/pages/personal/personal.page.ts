import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.page.html',
  styleUrls: ['./personal.page.scss'],
})
export class PersonalPage implements OnInit {

  public cours: any;

  textBuscar='';

  dataDoctores:any[]=[];
  dataEnfermeros:any[]=[];
  dataPersonas:any[]=[];
  dataLlamada:any[]=[];
  dataConsultaLlamada:any[]=[];

  datosDoctores: Array<any>=[];
  icons="";

  constructor( public http: HttpClient , public alertCtrl: AlertController ) { 
    this.icons="doctores";
  }

  ngOnInit() {



  }

  ionViewDidEnter(){
    this.MetodoGETPersonas();

    this.MetodoGETLlamada();
  }


  buscar(event){
    console.log(event);
    this.textBuscar=event.detail.value;
    console.log(event.detail.value);
  }

  DoctorLlamada(nombres,apellidos,id) {

    var ConfirmacionLlamada:boolean=false;
    var id_persona_eliminar=0;

    for (let items of this.dataLlamada) {
      if (items.id_persona == id) {
        
        ConfirmacionLlamada=true;
        id_persona_eliminar=items.id_turno;

      } 
    } 

    if(ConfirmacionLlamada==true){
      console.log('Se encontro que ya estaba en llamada');
      this.AdvertenciaLlamada(id_persona_eliminar);
    } else {
      this.RegistrarLlamada(nombres,apellidos,id);
      console.log('Aun no esta en llamada');
    }
    console.log('La confirmacion de la Llamada es :', ConfirmacionLlamada);





  }

  async AdvertenciaLlamada(id_persona){
    
    const alert = await this.alertCtrl.create({
      header: 'ADVERTENCIA',
      message: `<strong>El Doctor seleccionado ya se encuentra en rol llamada, desea quitarlo de llamada?</strong>` ,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass:'primary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.sendDeleteRequest(id_persona)
            console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }


  async RegistrarLlamada(nombres,apellidos,id){
    console.log('Entramos a RegistrarLlamada');
    var nombre='';
    var apellido='';
    var id_persona =0;

    id_persona = id;
    for(var i = 0; i< nombres.length;i++){
      if(nombres.charAt(i)!==' '){
        nombre = nombre + nombres.charAt(i);
      }else{
        i=apellidos.length+1;
      }
    }

    for(var i = 0; i< apellidos.length;i++){
      if(apellidos.charAt(i)!==' '){
        apellido = apellido + apellidos.charAt(i);
      }else{
        i=apellidos.length+1;
      }
    }

    const alert = await this.alertCtrl.create({
      header: 'Sistema de Notificacion',
      message: `<strong>Desea colocar al Dr. ${nombre} ${apellido} en llamada?</strong>` ,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass:'primary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Si',
          handler: () => {
            this.sendPostRequest(id_persona)
            console.log('Confirm Okay');
          }
        }
      ]
    });

    await alert.present();

  }

  sendDeleteRequest(id) {

    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );

    this.http.delete(`https://localhost:44347/api/llamada/${id}`).subscribe()

    setTimeout(() => {
      this.MetodoGETLlamada();
    },1000);

  }

  sendPostRequest(id) {

    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );


    let postData = {
            "id_persona": id,
    }
    this.http.post('https://localhost:44347/api/llamada', postData, { headers: headers}).subscribe()

    setTimeout(() => {
      this.MetodoGETLlamada();
    },1000);

  }

  doRefresh(event) {
    console.log('Begin async operation');
    this.MetodoGETLlamada();    

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }


  MetodoGETPersonas(){

    this.http.get<any[]>('https://localhost:44347/api/personas').subscribe(dataPersonas=>{
    
      this.dataPersonas=dataPersonas;
      console.log(dataPersonas);
    });

  }

  MetodoGETLlamada(){

    this.http.get<any[]>('https://localhost:44347/api/llamada').subscribe(dataLlamada=>{
    
      this.dataLlamada=dataLlamada;
      console.log('Este es el contenido de LLAMADA',dataLlamada);

      if(dataLlamada.length==0){
        console.log('NO SE ENCUENTRO NINGUN DOCTOR EN EL ROL DE LLAMADA');
      }else{
        console.log('YA HAY DOCTORES EN LLAMADA!!!')
      }
    });

  }



}
