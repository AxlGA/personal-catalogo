import { Pipe, PipeTransform } from '@angular/core';
import { stringify } from 'querystring';
import { Z_FILTERED } from 'zlib';
import { FileDetector } from 'protractor';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(arreglo: any[], texto: string): any[] {
    
   console.log('ESTO ES LO QUE RECIBE ',arreglo)
    if(texto==''){
      return arreglo;
    }

    arreglo=arreglo.filter( item => {
      return item.nombres.toLowerCase().includes( texto.toLocaleLowerCase() );

    });

    console.log('Este es el arreglo de vuelta',arreglo);
    console.log('Este es el texto entrante ',texto);

    return arreglo;
  }

}
